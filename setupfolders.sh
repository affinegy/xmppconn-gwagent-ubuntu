sudo useradd xmppconn

mkdir -p deb/opt/alljoyn/apps/xmppconn
mkdir -p deb/opt/alljoyn/apps/xmppconn/acls
mkdir -p deb/opt/alljoyn/apps/xmppconn/bin
mkdir -p deb/opt/alljoyn/apps/xmppconn/store
mkdir -p deb/opt/alljoyn/apps/xmppconn/etc

sudo chown -R xmppconn deb/opt/alljoyn/apps/xmppconn
sudo chgrp -R xmppconn deb/opt/alljoyn/apps/xmppconn

sudo chown -R xmppconn deb/opt/alljoyn/apps/xmppconn/acls
sudo chgrp -R xmppconn deb/opt/alljoyn/apps/xmppconn/acls

sudo chown -R xmppconn deb/opt/alljoyn/apps/xmppconn/bin
sudo chgrp -R xmppconn deb/opt/alljoyn/apps/xmppconn/bin

sudo chown -R xmppconn deb/opt/alljoyn/apps/xmppconn/store
sudo chgrp -R xmppconn deb/opt/alljoyn/apps/xmppconn/store

sudo chown -R xmppconn deb/opt/alljoyn/apps/xmppconn/etc
sudo chgrp -R xmppconn deb/opt/alljoyn/apps/xmppconn/etc
